---
title: "2316-SS"
output:
  html_document:
    df_print: paged
    toc: yes
  pdf_document:
    toc: yes
  word_document:
    toc: yes
editor_options: 
  chunk_output_type: console
---

# Análises para SS
```{r, include=FALSE}
knitr::opts_chunk$set(message = FALSE)
knitr::opts_chunk$set(warning = FALSE)
knitr::opts_chunk$set(fig.pos = 'H')

options(knitr.duplicate.label = 'allow')
options(SSipen = 999)

```

```{r dados, include=FALSE}

#importar planilha
library(readxl)
coringa <- read_excel("X:/Users/joaob/Google Drive/_UFSC/Analises estatisticas/Liandra-TCC/2316.xlsx",
                      sheet = "SR",
                      col_names = FALSE)

#colunas iniciais
colini = 2 #nº colunas iniciais (antes medidas) #trat + rep
names.colini <- c("trat", "rep")


#linhas iniciais
linini = 3 #nº linhas iniciais (antes das medidas)
ld <- 1 #linha em que está a data #dd/mm/aaaa
lh <- 2 #linha em que está a hora #hh:mm

#####--##--##--##--##--##--##--##--##--##--##--##--##--##--##--##--##--##
#mudar a eSSala da hora #dh
dh <- c()
for (j in seq(colini + 2, length(coringa), 3))
  #j = coluna com data/hora
{
  dh <- c(dh, as.numeric(coringa[ld, j]) + as.numeric(coringa[lh, j]))
}
dh <- round((dh-dh[1])*24,2)

#limpar o data.set
#retirar as duas colunas que antecedem a média (calculada no excel)
col.fica <- c(1:colini,seq(colini+3,length(coringa), by=3)) 
coringa <- coringa[col.fica]
#eliminar as linhas iniciais
coringa <- coringa[-(1:linini),]

#deScontar 0.5 do repicado e transformar os dados em numeric
coringa <- data.frame(coringa[,1:colini],data.matrix(coringa[(colini+1):length(coringa)])-0.5)

#nomear as colunas iniciais (trat+rep) e as demais com o tempo/hora
colnames(coringa) <- c(names.colini, dh)
```


```{r pass arguments, fig.height=3}

#calcular aaccm
source("aaccm-calc.R")

#último tempo para aaccm1
t.fim=93.5

#calcular aaccm
data.aaccm <- aaccm.calc(coringa, t.end = t.fim, colini)
data.aaccm$trat <- as.factor(data.aaccm$trat)
data.aaccm$aaccm <- data.aaccm$aaccm/t.fim



# data.aaccm <- data.aaccm[order(data.aaccm[1]),]

#calcular ivcm
# source("ivcm-calc.R")

#observações experimento 1
#1:4 = linear
#5:8 = linear
#9:12 = linear
#13:16 = linear
#17:20 = linear
#21:24 = linear
#25:28 = linear

# data.ivcm <- ivcm.linear.reg(coringa[c(1:28), ], t.end=t.fim)
# # data.ivcm <- rbind(data.ivcm,
# #                    ivcm.zero(coringa[c(5:8,9:12,13:16,25:28),],t.end=t.fim)
# #                    )
# 
# data.ivcm$trat <- as.factor(data.ivcm$trat)



#salvar uma planilha com os dados
# library(openxlsx)
# write.xlsx(data.frame(data.aaccm, ivcm = data.ivcm[, 3]),
#            file = "dados-SS.xlsx")
```

```{r child = 'aaccm.Rmd'}

```



```{r}
sessionInfo()
```

